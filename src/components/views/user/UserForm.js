import {Form, Field} from 'simple-react-forms';
import React, {Component} from 'react';
import { browserHistory } from 'react-router'
import {Button, ButtonToolbar} from 'react-bootstrap';

  let buttonAction = "";
  let buttonName = "";

class UserForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userData : [],
      message : "",
    };
  }


  componentWillMount() {

    const action = this.props.params.action;
    const id = this.props.params.id;

    if(action==='New'){
      buttonName = "Create";
      buttonAction = this.save.bind(this);

    }

    if(action==='Delete'){
      buttonName = "Delete";
      buttonAction = this.delete.bind(this);
      fetch('/api/users/'+id)
      .then(res => res.json())
      .then(json => {
        this.setState({ userData: json });
      });

    }

    if(action==='Update'){
      buttonName = "Save";
      buttonAction = this.update.bind(this);
      fetch('/api/users/'+id)
      .then(res => res.json())
      .then(json => {
        this.setState({ userData: json });
      });

    }

  }

  save () {
    let user = this.refs['simpleForm'].getFormValues();
    fetch('/api/users/', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
      })
      .then(res=>res.json()
    .then(console.log(res)));
      this.setState({message : "Kullanıcı Eklendi."});
      }


  update () {
    let user = this.refs['simpleForm'].getFormValues();
    fetch('/api/users/'+this.props.params.id, {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
      })
      .then(res=>res.json())

      this.setState({message : "Kullanıcı Güncellendi."})
      }

  delete () {
    let user = this.refs['simpleForm'].getFormValues();
    fetch('/api/users/'+this.props.params.id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
      })
      .then(res=>res.json())
      .then(res => console.log(res));
      this.setState({message : "Kullanıcı Silindi."})
      }

  render () {

    return (
    <div>
      {this.state.message}
      <Form ref='simpleForm'>
          <Field
            name='name'
            label='İsim'
            type='text'
            value={this.state.userData.name}

          />
          <Field
            name='lastname'
            label='Soyisim'
            type='text'
            value={this.state.userData.lastname}
          />
      </Form>
      <ButtonToolbar>
      <Button onClick={browserHistory.goBack}>Back</Button>
      <Button onClick={buttonAction} bsStyle="info">{buttonName}</Button>
      </ButtonToolbar>

    </div>
    );

  }
}
export default UserForm;
