import React, { Component } from "react";
import "bootswatch/journal/bootstrap.css";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {Button, ButtonToolbar} from 'react-bootstrap';
import Timestamp from 'react-timestamp';
import {browserHistory} from "react-router";


class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usersData : [],
      message : "",
    };
  }

  componentDidMount() {
    fetch('/api/users/')
    .then(res => res.json())
    .then(json => {
      this.setState({ usersData: json });
    });
  }


  userAction(action) {
    if(action==="New"){
      browserHistory.push('UserForm/New/');
    }else{
    let selectedUsers = this.refs.table.state.selectedRowKeys;
    if(selectedUsers.length<1){
      this.setState({ message:"Kullanıcı Seçin" });
    }else if (selectedUsers.length>1){
      this.setState({ message:"Sadece 1 Kullanıcı Seçin" });
    }else{
      browserHistory.push('UserForm/'+ action +'/' + selectedUsers);
    }
    }
  };

  dateFormatter (date) {
    return <Timestamp time={date/1000} format='full' />
  };

  detailButton(cell, row, enumObject, rowIndex){
    return <Button bsStyle="info" onClick={() => this.detailButtonAction(row)}>Detail</Button>
  };
  detailButtonAction(row) {
    browserHistory.push('UserDetail/'+row.id);
  };


  render() {
    const usersData = this.state.usersData;
    if (!usersData) return <div>Loading</div>;

    const options = {
      page: 1,  // which page you want to show as default
      sizePerPageList: [ {
        text: '5', value: 5
      }, {
        text: '10', value: 10
      }, {
        text: 'All', value: usersData.length
      } ], // you can change the dropdown list for size per page

      sizePerPage: 5,  // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3,  // the pagination bar size.
      prePage: 'Prev', // Previous page button text
      nextPage: 'Next', // Next page button text
      firstPage: 'First', // First page button text
      lastPage: 'Last', // Last page button text
      prePageTitle: 'Go to previous', // Previous page button title
      nextPageTitle: 'Go to next', // Next page button title
      firstPageTitle: 'Go to first', // First page button title
      lastPageTitle: 'Go to Last', // Last page button title
      paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
      paginationPosition: 'top'  // default is bottom, top and both is all available

    };

    let selectRowProp = {
      mode: "checkbox",
      clickToSelect: true,
      bgColor: "rgb(238, 193, 213)",
    };
    return (
      <div>
        {this.state.message}
        <ButtonToolbar>
          <Button bsStyle="success" onClick={() => this.userAction("New")}>New</Button>
          <Button bsStyle="warning" onClick={() => this.userAction("Update")}>Edit</Button>
          <Button bsStyle="danger" onClick={() => this.userAction("Delete")}>Delete</Button>
        </ButtonToolbar>

      <BootstrapTable isKey data={ usersData } pagination={ true } options={ options } selectRow={selectRowProp} ref='table' search >
          <TableHeaderColumn dataField='id' isKey={ true } width='30'>Id</TableHeaderColumn>
          <TableHeaderColumn dataField='name' dataSort={ true } width='100'>Name</TableHeaderColumn>
          <TableHeaderColumn dataField='lastname' dataSort={ true } width='100'>Lastname</TableHeaderColumn>
          <TableHeaderColumn dataField='createdAt' dataSort={ true } dataFormat={this.dateFormatter.bind(this)} width='100'>Created At</TableHeaderColumn>
          <TableHeaderColumn dataField='button' dataFormat={this.detailButton.bind(this)} width='10'>Detail</TableHeaderColumn>
      </BootstrapTable>
      </div>
    );
  }
}

export default UserList;
