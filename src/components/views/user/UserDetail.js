import {Form, Field} from 'simple-react-forms';
import React, {Component} from 'react';
import { browserHistory } from 'react-router'
import {Button, ButtonToolbar} from 'react-bootstrap';

class UserDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userData : [],
      message : "",
    };
  }


  componentWillMount() {
    const id = this.props.params.id;

      fetch('/api/users/'+id)
      .then(res => res.json())
      .then(json => {
        this.setState({ userData: json });
      });
  }

  update () {
    let user = this.refs['simpleForm'].getFormValues();
    fetch('/api/users/'+this.props.params.id, {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
      })
      .then(res=>res.json())

      this.setState({message : "Kullanıcı Güncellendi."})
      }

  delete () {
    let user = this.refs['simpleForm'].getFormValues();
    fetch('/api/users/'+this.props.params.id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
      })
      .then(res=>res.json());
      this.setState({message : "Kullanıcı Silindi."})
      }

  dateFormatter(date){
      let date1 = new Date(date);
      const timestamp = date1.toString();
      return timestamp;
  }

  render () {

    return (
    <div>
      {this.state.message}
      <Form ref='simpleForm'>
          <Field
            name='name'
            label='İsim'
            type='text'
            value={this.state.userData.name}

          />
          <Field
            name='lastname'
            label='Soyisim'
            type='text'
            value={this.state.userData.lastname}

          />
          <Field
            name='createdAt'
            label='Oluşturuldu'
            disabled
            type='text'
            value={this.dateFormatter(this.state.userData.createdAt)}
          />
          <Field
            name='updatedAt'
            label='Güncellendi'
            disabled
            type='text'
            value={this.dateFormatter(this.state.userData.updatedAt)}
          />
      </Form>
      <ButtonToolbar>
      <Button onClick={browserHistory.goBack}>Back</Button>
      <Button onClick={this.update.bind(this)} bsStyle="info">Save</Button>
      <Button onClick={this.delete.bind(this)} bsStyle="danger">Delete</Button>
      </ButtonToolbar>

    </div>
    );

  }
}
export default UserDetail;
