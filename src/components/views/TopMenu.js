import React, { Component } from "react";
import "bootswatch/journal/bootstrap.css";
// import "bootstrap/dist/css/bootstrap.css";

import { Navbar,Nav,NavItem,NavDropdown,MenuItem } from "react-bootstrap";
import {browserHistory} from "react-router";

let activeAction = null;

class TopMenu extends Component {

  constructor() {
    super();
    this.state = {
      activeAction : 1
    };
  }

  render() {
    return (
      <div>
        <Navbar>

            <Nav
            bsStyle="pills"
            stacked
            activeKey={activeAction}
            onSelect={i => {
                this.setState({ activeAction: i });
                i === 1 ? browserHistory.push('/') : null;
                i === 2 ? browserHistory.push('/UserList') : null;
              }}

            >
              <NavItem eventKey={1} >
                Anasayfa
              </NavItem>
              <NavItem eventKey={2} >
                Kullanıcılar
              </NavItem>

              <NavDropdown eventKey={3} title="Login/Register" id="basic-nav-dropdown">
                <MenuItem eventKey={3.1} href="/Login">Login</MenuItem>
                <MenuItem eventKey={3.2} href="/Register">Register</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey={3.3}>Separated link</MenuItem>
              </NavDropdown>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default TopMenu;
