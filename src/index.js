import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Route, Router, IndexRoute } from 'react-router'
import Root from './Root';
import UserForm from './components/views/user/UserForm'
import Home from './components/views/Home'
import UserDetail from './components/views/user/UserDetail'
import UserList from './components/views/user/UserList'
import Login from './components/views/login_reg/Login'
import Register from './components/views/login_reg/Register'
import Header from './components/views/Header'
import FourOFour from './404Example'

ReactDOM.render(<Header />, document.getElementById('header'))

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={Root}>
      <IndexRoute component={Home} />
      <Route path="/UserForm/:action/:id" component={UserForm} />
      <Route path="/UserForm/:action" component={UserForm} />
      <Route path="/UserDetail/:id" component={UserDetail} />
      <Route path="/UserList" component={UserList} />
      <Route path="/Home" component={Home} />
      <Route path="/Login" component={Login} />
      <Route path="/Register" component={Register} />
      <Route path="*" component={FourOFour}/>
    </Route>
  </Router>, document.getElementById('root'))
